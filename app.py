import flask
import flask_restful
from flask_cors import CORS
import flask_sqlalchemy
import os

if os.environ.get('FLASK_ENV') == 'prod':
    from prod_configs import Config
else:
    from configs import Config

app = flask.Flask(__name__)

app.config.from_object(Config)

db = flask_sqlalchemy.SQLAlchemy(app)
db.init_app(app)

CORS(app)

api = flask_restful.Api(app)
