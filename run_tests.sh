#!/usr/bin/env bash

python manage.py runserver &
last_pid=$!
sleep 3
python -m unittest
kill -KILL $last_pid
python manage.py cleanup_test 
