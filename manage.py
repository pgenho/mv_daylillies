from app import app, db
from flask import redirect, url_for
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from models import *
from apis.people_api import * 
from apis.plant_api import *
from apis.search_api import *
from admin.views import admin
from public.views import site
#from public.bundles import *
from auth import *  

from tests.cleanup import TestDBCleanup
from data.seed import MVDSeed, CreateAdmin

from flask_assets import Environment, Bundle

migrate = Migrate(app, db)

manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command('cleanup_test', TestDBCleanup())
manager.add_command('seed_db', MVDSeed())
manager.add_command('create_admin', CreateAdmin())

app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(site, url_prefix='/site')


assets = Environment(app)
assets.init_app(app)

public_vendor_css = Bundle(
    'site/node_modules/bootstrap/dist/css/bootstrap.css',
    output='css_min/vendor.css'
    )

public_mvd_css = Bundle(
    'site/css/mvd.css',
    output='css_min/mvd.css'
    )

public_vendor_js = Bundle(
    'site/node_modules/vue/dist/vue.min.js',
    'site/node_modules/jquery/dist/jquery.min.js',
    'site/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
    output='js_min/vendor.min.js'
    )

public_layout_custom_js = Bundle(
    'site/js/cart.js',
    output='js_min/mvd_core.min.js'
    )

public_home_js = Bundle(
    'site/js/home.js',
    'site/js/lightbox.js',
    output='js_min/home.min.js'
    )

public_shop_js = Bundle(
    'site/js/shop.js',
    output='js_min/shop.min.js'
    )


assets.register('vendor_css', public_vendor_css)
assets.register('mvd_css', public_mvd_css)
assets.register('public_vendor_js', public_vendor_js)
assets.register('public_custom_js', public_layout_custom_js)
assets.register('public_home_js', public_home_js)
assets.register('public_shop_js', public_shop_js)


@app.route('/', methods=['GET', 'POST'])
def index():
    return redirect(url_for('site.site_home'))

if __name__ == '__main__':
    manager.run()
