class UserEntry {
    //construct input, select etc. element from params.
    constructor(params) {
        this.params = params;
        this.elem = null;

        //construct base element
        switch(params.type) {
            case 'input':
                this.elem = $('<input>');
                break;
            case 'html':
                this.elem = $('<textarea class="html"></textarea>');
                break;
            case 'select':
                this.elem = $('<select></select>');
                break;
            case 'checkbox':
                this.elem = $('<input type="checkbox">');
                break;
            case 'textarea':
                this.elem = $('<textarea></textarea>');
                break;
            default:
                this.elem = $('<div></div>');
                break;
        }
        this._populate_attribs();
        this._populate_extra_attribs();
        this._populate_extra_classes();
        if (this.params.type === 'select') {
            this._populate_sel_options();
        };
        //this._populate_title_button();
    }

    //_populate_title_button() {
        //if (typeof(this.params.title_button_id) != undefined) {
            //console.log(this.elem);
            //$(this.elem).append(o);
            //title_button_id: 'addQtyCountPrice'[
        //}
    //}


    _populate_extra_classes() {
        if (this.params.extra_classes) {
            this.params.extra_classes.forEach( i => {
                $(this.elem).addClass(i);
            });
        };
    }


    _populate_sel_options() {
        let p = this.params;
        if (p.select_data) {
            p.select_data.forEach(i => {
                let o = $('<option></option>');
                $(o).attr('value', i.val); 
                $(o).text(i.name);
                $(this.elem).append(o);
            });
        }
        if (p.val) {
            $(this.elem).val(p.val);
        }
    }

    _populate_extra_attribs() {
        if (this.params.extra_attribs) {
            this.params.extra_attribs.forEach((v) => {
                $(this.elem).attr(v.id, v.val);
            });
        }
    }

    _populate_attribs() {
        let p = this.params;
        
        if (typeof(p.id) !== 'undefined') {
            $(this.elem)
                .attr('id', p.id)
                .attr('name', p.id)
        }
        if (typeof(p.type_type) !== 'undefined') {
            $(this.elem).attr('type', p.type_type);
        }
        if (typeof(p.val) !== 'undefined') {
            if (p.type === 'html') {
                //for html textareas.
                //set data on object, will re-insert in editor when instantiated.
                $(this.elem).data('editor_data', p.val);
            }
            else if (p.type === 'select') {
                //this gets set when options are populated.
            }
            else if (p.type_type === 'checkbox') {
                if (p.val === true) {
                    $(this.elem).attr('checked', true);
                }
            }
            else {
                $(this.elem).val(p.val);
            }
        }
        if (typeof(p.placeholder) !== 'undefined') {
            $(this.elem).attr('placeholder', p.placeholder);
        }

        $(this.elem).addClass('form-control');
    }
};


//Admin Utilities 
(function(Amu, $, undefined) {

    Amu.populate_modal = function(modal, params) {
        //populate user entries into model popup.
        
        let form = $('<form></form>');

        params.entries.forEach(function(params) {
            let e;
            let fg = $('<div class="form-group"></div>');

            if (params.extra_formgroup_classes) {
                $(fg).addClass(params.extra_formgroup_classes);
            }

            $(fg).append('<label>' + params.title + '</label>'); 

            if (params.title_button_id) {
                let btn = $('<button class="btn-lb btn-plus btn btn-sm" type="button"></button>');
                $(btn).attr('id', params.title_button_id);
                $(fg).find('label').after(btn);
            }

            if (params.type == 'jq_elem') {
                $(fg).find('label').remove();
                //premade html element
                e = params;
            }
            else {
                e = new UserEntry(params);
            }

            $(fg).append(e.elem);

            $(form).append(fg);
        });

        $(modal)
            .find('.modal-header .modal-title')
            .empty()
            .append(params.title);

        $(modal)
            .find('.modal-body')
            .empty()
            .append(form);

        $(modal)
            .find('.modal-footer .btn-primary')
            .text(params.submit_text)

        $(modal).on('shown.bs.modal', function() {
            //instantiate wsywig on on .html elements
            tinymce.init({
                selector: '.html',
                plugins: ['link', 'lists'],
                menubar: 'insert',
                anchor_bottom: false,
                anchor_top: false,
                toolbar: [
                    'undo redo | styleselect | bold italic | alignleft aligncenter alignright', 
                    'link | numlist | bullist'
                ],
                branding: false,
                link_assume_external_targets: true,
                default_link_target: "_blank",
                setup: function(editor) {
                    editor.on('init', function() {
                        //restore content if <textarea> element has data
                        let area = $('#' + editor.id);
                        let bdata = $(area).data().editor_data;

                        if ($(area).length && bdata) {
                            editor.setContent(bdata);
                        }
                    });
                }
            });
        });

        $(modal).on('hide.bs.modal', function() {
            //destroy tinymces
            tinymce.remove('.html');
        });

        return modal;
    };


    Amu.modal_alert = function(modal, content, extra_classes) {
        let alrt = $('<div role="alert" class="alert modal-alert created_mod_alert"></div>');
        let close_button = `<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>`;

        $(alrt).append(close_button, content);

        extra_classes.forEach(function(v) {
            $(alrt).addClass(v);
        });

        $(modal)
            .find('.created_mod_alert')
            .remove();

        $(modal)
            .find('.modal-body')
            .append(alrt);
    };


    Amu.ab_disab = function(element, state) {
        $(element)
            .attr('disabled', state)
            .toggleClass('disabled')
    };


    Amu.cover_wait_add = function() {
        //background cover with wait spin.
        let cvr = $('<div class="cover-wait"><h2><i class="fa fa-cog fa-spin"></i></h2></div>');
        $('body').append(cvr); 
    };


    Amu.cover_wait_remove = function() {
        $('.cover-wait').remove();
    };


}(window.Amu = window.Amu || {}, $));
