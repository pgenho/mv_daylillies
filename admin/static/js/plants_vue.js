var vue = new Vue({
    el: '#app',
    data: {
        upload_error: null
    },
    delimiters: ['[[', ']]'],
    methods: {
        showModal() {
            plant.add_new();
        },
        priceTemplate() {
            window.open('price_list_template');
        },
        showUploadModal() {
            this.upload_error = null;
            $('#fileform input').val('');
            $('#upload_modal').modal('show');
        },
        submitUpload() {
            var that = this;

            var form_data = new FormData(document.getElementById('fileform'));

            $.ajax({
                url: 'upload_price_list',
                type: 'POST',
                data: form_data, 
                processData: false,
                contentType: false,
                success: function(rslt) {
                    window.location.reload(true)
                },
                error: function(rslt) {
                    that.upload_error = rslt.responseText
                }
            });
        }
    }
})
