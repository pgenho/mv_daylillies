
class PlantTable {
    constructor() {
        this.table_holder = $('#plant-table'); 
        this.table = null;
    };

    _table_buttons() {
        let bc = $('<div class="cell-buttons"></div>');
        $(bc).append('<div class="icn edit-icn"><i class="fas fa-edit fa-lg"></i></div>');
        $(bc).append('<div class="icn delete-icn"><i class="fas fa-trash fa-lg"></i></div>');
        return $(bc).get(0).outerHTML;
    };

    _bind_row_icons() {
        let scope = this;
        let table = $(this.table_holder)
                        .find('table')
                        .first()
                        .DataTable();

        table.rows().every(function(i, t, r) {
            let row = this;
            let button_cell = $(row.node()).find('td').first();
            
            $(button_cell).find('.delete-icn').each(function() {
                $(this)
                    .unbind('click')
                    .on('click', function() {
                        scope._delete_entry(row);
                    });
            });

            $(button_cell).find('.edit-icn').each(function() {
                $(this)
                    .unbind('click')
                    .on('click', function() {
                        window.plant.edit_entry(row);
                    });
            });

        });
    };

    _delete_entry(dt_row) {
        let row = $(dt_row.node());
        let d = $(row).data().base_data
        let id = d.id; 
        let conf = `Delete ${d.name}?`;

        if (typeof(id) === 'undefined') {
            return
        }

        $.bsAlert.confirm(conf, function() {
            $.ajax({
                method: 'DELETE',
                url: '/api/plant/' + id, 
                success:(rslt) => {
                    dt_row.remove().draw();
                },
                error: function(e) {
                }
            });
        });
    };

    _render_table(data) {

        let that = this;
        let existing_table = $(this.table_holder).find('table').first();
        if (existing_table.length) {
            let old = $(existing_table).DataTable();
            old.destroy();
            $(this.table_holder).empty();
        }

        let t = $('<table class="data-table table-striped atable" style="width: 100%"></table>');
        let columns = [
            {title: ''},
            {title: 'Name'},
            {title: 'Photo Count'},
            {title: 'Show On Site'},
            {title: 'Front Page'},
            //{title: 'Added'},
            {title: 'Updated'},
        ]

        $(this.table_holder)
            .empty()
            .append(t);

        this.table = $(t).DataTable({
            columns: columns,
            columnDefs: [
                {
                    targets: 0,
                    width: '60px',
                    orderable: false,
                    searchable: false,
                    render: function() {
                        return that._table_buttons();
                    }
                }
            ],
            drawCallback: () => this._bind_row_icons(),
            order: [[1, 'asc'],],
            lengthMenu: false,
            lengthChange: false,
            pageLength: 25
        });

        $.each(data, (i,v) => {
            this.add_table_row(v);
        });

        this.table.state.clear();
        this.table.draw();
    }

    add_table_row(v) {
        let data = ['', 
                    v.name, 
                    v.photo_count, 
                    v.show_on_site,
                    v.show_on_frontpage,
                    v.time_updated
                    ];
        let row = this.table.row.add(data);
        $(row.node()).data('base_data', v);
        return row;
    }

    load_table() {
        $.ajax({
            method: 'GET',
            url: '/api/plants', 
            success:(rslt) => {
                this._render_table(rslt);
            },
        });
    }
}


class Plant {
    constructor() {
        this.modal = $('#admin-modal');
        this.new_modal_created = false;
        this.sel_categories = null;
        this.active_categories = [];
        this.active_qty_count_prc = {'names': [], 'data': []};
        this.photos_data = null;
    }

    _get_params() {
        return {
            title: '<b>New Item</b>',
            submit_callback: this._submit_callback,
            submit_text: 'Submit',
            entries: [
                {
                    id: 'new-plant-name', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>Name</b>',
                    extra_formgroup_classes: 'fg-inline fg-inline-2',
                    placeholder: 'name'
                },
                {
                    id: 'new-plant-category',
                    type: 'select',
                    title: '<b>Categories</b>',
                    title_button_id: 'addCategory',
                    extra_formgroup_classes: 'fg-inline fg-inline-2',
                    select_data: this.sel_categories,
                },
                {
                    type: 'jq_elem',
                    elem: '<div class="form-multi row"><div class="col-md-6 offset-md-6" id="categories-multi"></div></div>',
                },
                {
                    type: 'jq_elem',
                    elem: '<br>',
                },
                {
                    id: 'new-sub-item-name', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>Desc. / Qty Avail. / Price</b>',
                    title_button_id: 'addQtyCountPrice',
                    placeholder: 'unique descriptor', 
                    extra_formgroup_classes: 'fg-inline fg-inline-3',
                    status: 'disabled'
                },
                {
                    id: 'new-plant-count', 
                    type: 'input',
                    type_type: 'number',
                    placeholder: 'qty available', 
                    title: '',
                    val: 0,
                    extra_formgroup_classes: 'fg-inline fg-inline-3',
                    status: 'disabled'
                },
                {
                    id: 'new-plant-price', 
                    type: 'input',
                    type_type: 'number',
                    placeholder: 'price per unit', 
                    title: '',
                    extra_formgroup_classes: 'fg-inline fg-inline-3',
                    status: 'disabled'
                },
                {
                    type: 'jq_elem',
                    elem: '<div class="form-multi" id="price-qty-ct-multi"></div>',
                },
                {
                    type: 'jq_elem',
                    elem: '<br>',
                },
                {
                    id: 'new-on-site', 
                    type: 'input',
                    type_type: 'checkbox',
                    val: false,
                    extra_classes: ['modal-checkbox'],
                    extra_formgroup_classes: 'fg-inline fg-inline-3',
                    title: '<b>Show On Site</b>',
                },
                {
                    id: 'new-on-frontpage', 
                    type: 'input',
                    type_type: 'checkbox',
                    val: false,
                    extra_classes: ['modal-checkbox'],
                    extra_formgroup_classes: 'fg-inline',
                    title: '<b>Show On Front Page</b>',
                },
                {
                    id: 'new-plant-html', 
                    type: 'html',
                    title: '<b>Item Description</b>'
                },
                {
                    id: 'modal-photo-upload',
                    type: 'jq_elem',
                    elem: this._modal_photos(),
                    title: '<b>Photo Upload</b>'
                }
            ]
        }
    };

    _set_dmUploader(droparea) {
	    let da = $(droparea).dmUploader({
            url: '/admin/upload_plant_photo',
            maxFileSize: 1024 * 1024 * 15,
            extFilter: ["jpg", "jpeg", "png", "gif", "tiff", "JPG", "JPEG", "PNG", "GIF", "TIFF"],
            dataType: 'json',
            onFileSizeError: function(file) {
                alert('File too big. Try a smaller one?');
            },
            onFileExtError: function() {
                alert('bad file type');
                $(droparea).dmUploader('reset')
            },
            onBeforeUpload: function() {
                Amu.cover_wait_add();
            },
            extraData: {
            },
            onComplete: function() {
                Amu.cover_wait_remove();
            },
            onUploadSuccess: (id, data) => {
                if (data.url_thumb) {
                    $(droparea).dmUploader('destroy')
                    this._replace_drop_photo(data, droparea);                   
                }
            },
			onUploadError: function() {
			},
			onUploadProgress: function(id, percent) {
			}
        });
    };

    _replace_drop_photo(data, droparea) {
        let img = $('<img class="upload-thumb"></img>');
        $(img).prop('src', data.url_thumb);
        $(droparea)
            .empty()
            .append(img);
        $(droparea)
            .addClass('has-photo')
            .data('photo_data', data);
        this._check_featured();
    };

    _confirm_remove_photo(box) {
        let scope = this;
        let conf = 'Delete this photo?';
        $.bsAlert.confirm(conf, function() {
            let id = 'none'
            let d = $(box)
                        .find('.photo-drop')
                        .data()
                        .photo_data;

            if (typeof(d.id) != 'undefined') {
                id = d.id;
            }

            $.ajax({
                method: 'DELETE',
                url: '/api/update_photo/' + id,
                data: d, 
                success:(rslt) => {
                    $(box).remove();
                    scope._check_featured();
                },
                error: function(e) {
                }
            });
        });
    };

    _remove_photo(box) {
        if ($(box)
                .find('.photo-drop')
                .first()
                .hasClass('has-photo')) {
                    this._confirm_remove_photo(box);                
                }
        else {
            $(box).remove();
        }
        this._check_featured();
    };

    _check_featured() {
        let has_check = false;
        $('.main-photo-check input')
            .each(function() {
                let img = $(this)
                            .closest('.photo-upload-panel')
                            .find('img')
                            .first()
                if (!img.length) {
                    return
                }
                if ($(this).is(':checked')) {
                    has_check = true;
                }
            });
        if (! has_check) {
            $('.main-photo-check input')
                .first()
                .prop('checked', true);
        };
    };

    _photo_upload_area(photo_row, isNew=true) {
        let box = $('<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 photo-upload-panel"></div>');
        let btnrw = $('<div class="photup-button-row"></div>');
        let rmv = $('<span class="rmv-upload pointer" title="Delete/Remove"><i class="fa fa-times-circle"</span>');
        let maincheck = $('<span class="main-photo-check"><input type="checkbox"> Feature</span>');
        let inr = $('<div class="photo-drop"></div>');
        let note = $('<h5>Drag and Drop Photo<h5/><div>or</div>');
        let inpt = $('<input type="file" title="select photo" style="display:none;">');
        let inpid = Math.random(); 
        $(inpt).attr('id', inpid);
        let lb = $('<label class="btn btn-sm">Select From Files</label>')
        $(lb).attr('for', inpid);
        let inptbx = $('<div class="sel-photo"></div>');
        let titlerow = $('<div class="photup-title-row"></div>');
        let title_ = $('<input class="photo-title" placeholder="Title (optional)"></div>');
        let desc = $('<input class="photo-desc" placeholder="Desc. (optional)"></div>'); 

        $(titlerow).append(title_, desc);
        $(btnrw).append(maincheck, rmv); 
        $(inptbx).append(lb, inpt);
        $(inr).append(note, inptbx);
        $(box).append(btnrw, inr, titlerow);
        $(photo_row).append(box);

        $(maincheck).find('input').change(function() {
            if (this.checked) {
                $('.main-photo-check input')
                    .not(this)
                    .prop('checked', false);
                }
            else {
                $('.main-photo-check input')
                    .first()
                    .prop('checked', true);
            }
        });

        $(rmv).on('click', () => {
            this._remove_photo(box);
        });

        if (isNew) {
            this._set_dmUploader(inr);
        };

        return box;
    };

    _modal_photos() {
        //create photo upload area and button.
        let root = $('<div></div>');
        let button_row = $('<div class="row"></div>');
        let col = $('<div class="col-md-12"></div>');
        let button = $('<button type="button" class="btn-lb btn-plus btn btn-sm">Add Photo</button>');
        let photo_row = $('<div class="row modal-photo-row"></div>');

        $(button).on('click', () => {
            this._photo_upload_area(photo_row, true);
        });

        $(col).append(button);
        $(button_row).append(col);
        $(root).append(button_row, photo_row);

        if (this.photos_data) {
            //redraw photoss
            this.photos_data.forEach((v) => {
                let box = this._photo_upload_area(photo_row, false);
                this._replace_drop_photo(
                    v, 
                    $(box).find('.photo-drop').first()
                    )
                $(box)
                    .find('.photo-title')
                    .val(v.title);
                $(box)
                    .find('.photo-desc')
                    .val(v.description);
                if (v.is_featured) {
                    $(box)
                        .find('.main-photo-check input:checkbox')
                        .prop('checked', true)
                };
            });
        }

        return root;
    };

    _get_modal_form() {
        //get photo data
        let photos_data = [];
        let show_on_site = $('#new-on-site').is(':checked');
        let show_on_frontpage = $('#new-on-frontpage').is(':checked');

        $('.photo-upload-panel')
            .each(function() {
                let dropdata = $(this)
                                    .find('.photo-drop')
                                    .first()
                                    .data('photo_data')

                if (typeof(dropdata) === 'undefined') {
                    return;
                }

                let checked = $(this)
                                .find('.main-photo-check input')
                                .first()
                                .is(':checked');

                let title = $(this)
                                .find('.photo-title')
                                .first()
                                .val();

                let desc = $(this)
                                .find('.photo-desc')
                                .first()
                                .val();

                photos_data.push({
                    file_data: dropdata,
                    main_photo: checked,
                    title: title, 
                    description: desc
                })
            });
        
        return {
            name: $('#new-plant-name').val(),
            //category: $('#new-plant-category').val(),
            categories: this.active_categories,
            subitem_count_price:  this.active_qty_count_prc.data,
            //price: $('#new-plant-price').val(),
            //count_available: $('#new-plant-count').val(),
            show_on_site: $('#new-on-site').is(':checked'),
            show_on_frontpage: $('#new-on-frontpage').is(':checked'),
            html: tinymce.get('new-plant-html').getContent(),
            photos: photos_data 
        };
    };

    _new(modal) {
        let scope = this;
        let data = this._get_modal_form();

        $.ajax({
            method: 'POST',
            url: '/api/plants', 
            data: {'data': JSON.stringify(data)},
            success:(rslt) => {
                let row = window.plant_table.add_table_row(rslt);
                row.draw();
                Amu.ab_disab(scope.mod_submit, false);
                scope.modal.modal('hide')
            },
            error:(e) => {
                let msg = '<b>Error saving changes: </b>' + e.responseJSON.message; 
                Amu.modal_alert(scope.modal, msg, ['alert-danger']);
                Amu.ab_disab(scope.mod_submit, false);
            }
        });
    };

    _update(modal, id, row) {
        let scope = this;
        let data = this._get_modal_form();

        $.ajax({
            method: 'PUT',
            url: '/api/plant/' + id, 
            data: {'data': JSON.stringify(data)},
            success:(rslt) => {
                row.remove().draw()
                let new_row = window.plant_table.add_table_row(rslt);
                new_row.draw();
                Amu.ab_disab(scope.mod_submit, false);
                scope.modal.modal('hide')
            },
            error:(e) => {
                let msg = '<b>Error saving changes: </b>' + e.responseJSON.message; 
                Amu.modal_alert(scope.modal, msg, ['alert-danger']);
                Amu.ab_disab(scope.mod_submit, false);
            }
        });
    };

    input_modal(action, params, id, row, callback) {
        this.modal = Amu.populate_modal(this.modal, params, callback);
        this.modal.modal({
            backdrop: 'static'
        });

        this.mod_submit = $(this.modal).
                                find('.modal-footer .btn-primary');
        $(this.mod_submit)
            .unbind('click')
            .on('click', () => {
                Amu.ab_disab(this.mod_submit, true);
                if (action == 'edit') {
                    this._update(this.modal, id, row);
                }
                else {
                    this._new(this.modal);
                }
            })

        let newcat = $(this.modal).
                            find('#addCategory');
        if (newcat.length) {
            $(newcat).on( 'click', () => {
                this._add_category();
            });
        };

        let newitem = $(this.modal).
                            find('#addQtyCountPrice');
        if (newitem.length) {
            $(newitem).on('click', () => {
                this._add_iqp();
            });
        };
    };

    _add_iqp() {
        //item, qty available, price
        let container = $('#price-qty-ct-multi');
        let name = $('#new-sub-item-name').val();
        let price = $('#new-plant-price').val();
        let count = $('#new-plant-count').val();

        if (count.length < 1 || price.length < 1 || name.length < 1) {
            return;
        }

        let group = [name, count, price];
        let txt = group.join(' | ');

        if (this.active_qty_count_prc.names.indexOf(name) == -1) {
            this.active_qty_count_prc.names.push(name);
            this.active_qty_count_prc.data.push(group);
        }
        else {
            return;
        }

        let elem = $('<div class="modal-multi-sel"></div>');
        let btn = $('<span class="modal-multi-del"><i class="fa fa-trash-alt"></i></span>'); 
        $(elem).append(txt, btn);

        $(container).append(elem)

        $(btn).on('click', () => {
            let i = this.active_qty_count_prc.names.indexOf(name);
            this.active_qty_count_prc.names.splice(i, 1);
            this.active_qty_count_prc.data.splice(i, 1);
            $(elem).remove();
        });
    };

    _add_category() {
        let cat = $('#new-plant-category');
        let txt = $( "#new-plant-category option:selected" ).text();
        let val = $(cat).val();
        let container = $('#categories-multi');

        if (this.active_categories.indexOf(val) == -1) {
            this.active_categories.push(+val);

            let elem = $('<div class="modal-multi-sel"></div>');
            let btn = $('<span class="modal-multi-del"><i class="fa fa-trash-alt"></i></span>'); 

            $(elem).append(txt, btn);

            $(container).append(elem)

            $(btn).on('click', () => {
                this.active_categories.splice(
                    this.active_categories.indexOf(val), 1 
                    );
                $(elem).remove();
            });
        }
    };

    _populate_categories(plant_cats, all_cats) {
        let container = $('<div class="form-multi row"><div class="col-md-6 offset-md-6" id="categories-multi"></div></div>');
        let b = $(container).find('#categories-multi');

        let cat_name = '';
        $.each(plant_cats, (i, x) => {
            $.each(all_cats, (j, y) => {

                if (x.category_id == y.val) {
                    cat_name = y.name;

                    if (this.active_categories.indexOf(y.val) == -1) {
                        this.active_categories.push(y.val);
                    }

                    let elem = $('<div class="modal-multi-sel"></div>');
                    let btn = $('<span class="modal-multi-del"><i class="fa fa-trash-alt"></i></span>'); 

                    $(elem).append(cat_name, btn);

                    $(b).append(elem)

                    $(btn).on('click', () => {
                        this.active_categories.splice(
                            this.active_categories.indexOf(cat_name), 1 
                            );
                        $(elem).remove();
                    });
                }
            });
        });

        return container;
    };

    _populate_subitem_price_qty(subitem) {
        let container = $('<div class="form-multi" id="price-qty-ct-multi"></div>');

        $.each(subitem, (i, v) => {

            let name = v.item;

            let group = [v.item, v.qty_available, v.price];
            let txt = group.join(' | ');

            if (this.active_qty_count_prc.names.indexOf(name) == -1) {
                this.active_qty_count_prc.names.push(name);
                this.active_qty_count_prc.data.push(group);
            }

            let elem = $('<div class="modal-multi-sel"></div>');
            let btn = $('<span class="modal-multi-del"><i class="fa fa-trash-alt"></i></span>'); 
            $(elem).append(txt, btn);

            $(container).append(elem)

            $(btn).on('click', () => {
                let i = this.active_qty_count_prc.names.indexOf(name);
                this.active_qty_count_prc.names.splice(i, 1);
                this.active_qty_count_prc.data.splice(i, 1);
                $(elem).remove();
            });
        });

        return container;
    };

    _show_edit(row) {
        let data = $(row.node()).data().base_data;

        this.photos_data = data.photos;

        let params = window.plant._get_params();

        params.title = '<b>Edit ' + data.name + '</b>';

        params.entries[0].val = data.name; 
        params.entries[2].elem = this._populate_categories(data.categories, params.entries[1].select_data); 
        params.entries[7].elem = this._populate_subitem_price_qty(data.subitem_count_prices);
        params.entries[9].val = data.show_on_site;
        params.entries[10].val = data.show_on_frontpage;
        params.entries[11].val = data.html;

        this.input_modal('edit', params, data.id, row);
    };

    _update_categories() {
        //get categories for dropdown
        return new Promise((resolve, reject) => {
            $.getJSON('/api/category') 
                .done((categories) => {
                    this.sel_categories = categories.map(
                    x => {
                        let a = {}; 
                        a.name = x.name, 
                        a.val = x.id;
                        return a;
                        }
                    )
                    resolve(this.sel_categories)
                });
            })
    };
            
    edit_entry(row) {
        return this._update_categories() 
            .then((cats) => {
                return this._show_edit(row); 
            })
    };

    add_new() {
        this.photos_data = null;
        return this._update_categories() 
            .then((cats) => {
                return this.input_modal('new', this._get_params());
            })
    };
}


(function() {
    window.plant = new Plant();
    window.plant_table = new PlantTable();

    plant_table.load_table();

    //$('#add-plant').click(function() {
        //plant.add_new();
    //});

}());
