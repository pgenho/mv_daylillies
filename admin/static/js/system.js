
class SystemUserTable {
    constructor() {
        this.table_holder = $('#system-table'); 
        this.table = null;
    };

    _table_buttons() {
        let bc = $('<div class="cell-buttons"></div>');
        $(bc).append('<div class="icn edit-icn"><i class="fas fa-edit fa-lg"></i></div>');
        $(bc).append('<div class="icn delete-icn"><i class="fas fa-trash fa-lg"></i></div>');
        return $(bc).get(0).outerHTML;
    };

    _bind_row_icons() {
        let scope = this;
        let table = $(this.table_holder)
                        .find('table')
                        .first()
                        .DataTable();

        table.rows().every(function(i, t, r) {
            let row = this;
            let button_cell = $(row.node()).find('td').first();
            
            $(button_cell).find('.delete-icn').each(function() {
                $(this)
                    .unbind('click')
                    .on('click', function() {
                        scope._delete_entry(row);
                    });
            });

            $(button_cell).find('.edit-icn').each(function() {
                $(this)
                    .unbind('click')
                    .on('click', function() {
                        window.system_user.edit_entry(row);
                    });
            });
        });
    };

    _delete_entry(dt_row) {
        let row = $(dt_row.node());
        let d = $(row).data().base_data
        let id = d.id; 
        let conf = `Delete admin ${d.username}?`;

        if (typeof(id) === 'undefined') {
            return
        }

        $.bsAlert.confirm(conf, function() {
            $.ajax({
                method: 'DELETE',
                url: '/api/user/' + id, 
                success:(rslt) => {
                    if (rslt && rslt.error) {
                        alert(rslt.error);
                        return;
                    }
                    dt_row.remove().draw();
                },
                error: function(e) {
                }
            });
        });
    };

    _render_table(data) {
        let that = this;
        let existing_table = $(this.table_holder).find('table').first();
        if (existing_table.length) {
            let old = $(existing_table).DataTable();
            old.destroy();
            $(this.table_holder).empty();
        }

        let t = $('<table class="data-table table-striped atable" style="width: 100%"></table>');
        let columns = [
            {title: ''},
            {title: 'firstname'},
            {title: 'lastname'},
            {title: 'username'},
            {title: 'email'},
            {title: 'last login'}
        ]

        $(this.table_holder)
            .empty()
            .append(t);

        this.table = $(t).DataTable({
            columns: columns,
            columnDefs: [
                {
                    targets: 0,
                    width: '60px',
                    orderable: false,
                    searchable: false,
                    render: function() {
                        return that._table_buttons();
                    }
                }
            ],
            drawCallback: () => this._bind_row_icons(),
            order: [[1, 'asc'],],
            lengthMenu: false,
            lengthChange: false,
            pageLength: 25
        });

        $.each(data, (i,v) => {
            this.add_table_row(v);
        });

        this.table.state.clear();
        this.table.draw();
    }

    add_table_row(v) {

        let data = ['', v.firstname, v.lastname, v.username, v.email, v.last_login];
        let row = this.table.row.add(data);
        $(row.node()).data('base_data', v);
        return row;
    }

    load_table() {
        $.ajax({
            method: 'GET',
            url: '/api/users', 
            success:(rslt) => {
                this._render_table(rslt);
            },
        });
    }
}

//id = db.Column(db.Integer, primary_key=True)
//username = db.Column(db.Unicode, nullable=False, unique=True)
//firstname = db.Column(db.Unicode, nullable=False)
//lastname = db.Column(db.Unicode, nullable=False)
//email = db.Column(db.String, nullable=True)
//password = db.Column(db.String(128), nullable=False)
//last_login = db.Column(db.DateTime, default=datetime.datetime.now)
//time_added = db.Column(db.DateTime, default=datetime.datetime.now)
//time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)


class SystemUser {
    constructor() {
        this.modal = $('#admin-modal');
        this.new_modal_created = false;

        this.params = {
            title: '<b>New System Admin</b>',
            //submit_callback: this._submit_callback,
            submit_text: 'Submit',
            entries: [
                {
                    id: 'new-admin-firstname', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>First Name</b>',
                    placeholder: 'firstname'
                },
                {
                    id: 'new-admin-lastname', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>Last Name</b>',
                    placeholder: 'lastname'
                },
                {
                    id: 'new-admin-username', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>Username</b> <span class="small">* must be unique.</span>',
                    placeholder: '',
                    extra_attribs: [{
                                    id: 'autocomplete',
                                    val: 'off'}
                                    ]
                },
                {
                    id: 'new-admin-email', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>Email</b>',
                    placeholder: 'bill@gmail.com',
                    extra_attribs: [{
                                    id: 'autocomplete',
                                    val: 'off'}
                                    ]
                },
                {
                    id: 'new-admin-password',
                    type: 'input',
                    //type_type: 'password',
                    type_type: 'text',
                    title: '<b>Password</b>',
                    placeholder: '',
                    extra_attribs: [{
                                    id: 'autocomplete',
                                    val: 'new-password'},
                                ]
                }
            ],
        }
    };

    _get_modal_form() {
        return {
            firstname: $('#new-admin-firstname').val(),
            lastname: $('#new-admin-lastname').val(),
            username: $('#new-admin-username').val(),
            email: $('#new-admin-email').val(),
            password: $('#new-admin-password').val(),
        };
    };

    _new(modal) {
        let scope = this;

        $.ajax({
            method: 'POST',
            url: '/api/add_user', 
            data: this._get_modal_form(),
            success:(rslt) => {
                let row = window.system_user_table.add_table_row(rslt);
                row.draw();
                Amu.ab_disab(scope.mod_submit, false);
                scope.modal.modal('hide')
            },
            error:(e) => {
                let msg = '<b>Error saving changes: </b>' + e.responseJSON.message; 
                Amu.modal_alert(scope.modal, msg, ['alert-danger']);
                Amu.ab_disab(scope.mod_submit, false);
            }
        });
    };

    _update(modal, id, row) {
        let scope = this;

        $.ajax({
            method: 'PUT',
            url: '/api/user/' + id, 
            data: this._get_modal_form(),
            success:(rslt) => {
                row.remove().draw()
                let new_row = window.system_user_table.add_table_row(rslt);
                new_row.draw();
                Amu.ab_disab(scope.mod_submit, false);
                scope.modal.modal('hide')
            },
            error:(e) => {
                let msg = '<b>Error saving changes: </b>' + e.responseJSON.message; 
                Amu.modal_alert(scope.modal, msg, ['alert-danger']);
                Amu.ab_disab(scope.mod_submit, false);
            }
        });
    };

    input_modal(action, params, id, row) {

        this.modal = Amu.populate_modal(this.modal, params);
        this.modal.modal({
            backdrop: 'static'
        });
        this.mod_submit = $(this.modal).
                                find('.modal-footer .btn-primary');
        $(this.mod_submit)
            .unbind('click')
            .on('click', () => {
                console.log(action);
                Amu.ab_disab(this.mod_submit, true);
                if (action == 'edit') {
                    this._update(this.modal, id, row);
                }
                else {
                    this._new(this.modal);
                }
            })
    };

    edit_entry(row) {
        let data = $(row.node()).data().base_data;
        let params = this.params;

        params.entries[0].val = data.firstname; 
        params.entries[1].val = data.lastname; 
        params.entries[2].val = data.username; 
        params.entries[3].val = data.email; 
        this.input_modal('edit', params, data.id, row);
    };

    add_new() {
        this.input_modal('new', this.params);
    };
}

(function() {
    window.system_user = new SystemUser();
    window.system_user_table = new SystemUserTable();
    system_user_table.load_table();
    $('#add-user').click(function() {
        system_user.add_new();
    });

}());
