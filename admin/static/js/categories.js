
class CategoryTable {
    constructor() {
        this.table_holder = $('#cat-table'); 
        this.table = null;
    };

    _table_buttons() {
        let bc = $('<div class="cell-buttons"></div>');
        $(bc).append('<div class="icn edit-icn"><i class="fas fa-edit fa-lg"></i></div>');
        $(bc).append('<div class="icn delete-icn"><i class="fas fa-trash fa-lg"></i></div>');
        return $(bc).get(0).outerHTML;
    };

    _bind_row_icons() {
        let scope = this;
        let table = $(this.table_holder)
                        .find('table')
                        .first()
                        .DataTable();

        table.rows().every(function(i, t, r) {
            let row = this;
            let button_cell = $(row.node()).find('td').first();
            
            $(button_cell).find('.delete-icn').each(function() {
                $(this)
                    .unbind('click')
                    .on('click', function() {
                        scope._delete_entry(row);
                    });
            });

            $(button_cell).find('.edit-icn').each(function() {
                $(this)
                    .unbind('click')
                    .on('click', function() {
                        window.category.edit_entry(row);
                    });
            });
        });
    };

    _delete_entry(dt_row) {
        let row = $(dt_row.node());
        let d = $(row).data().base_data
        let id = d.id; 
        let conf = `Delete category ${d.name}?`;

        if (typeof(id) === 'undefined') {
            return
        }

        $.bsAlert.confirm(conf, function() {
            $.ajax({
                method: 'DELETE',
                url: '/api/category/' + id, 
                success:(rslt) => {
                    dt_row.remove().draw();
                },
                error: function(e) {
                }
            });
        });
    };

    _render_table(data) {
        let that = this;
        let existing_table = $(this.table_holder).find('table').first();
        if (existing_table.length) {
            let old = $(existing_table).DataTable();
            old.destroy();
            $(this.table_holder).empty();
        }

        let t = $('<table class="data-table table-striped atable" style="width: 100%"></table>');
        let columns = [
            {title: ''},
            {title: 'priority'},
            {title: 'name'},
            {title: 'description'}
        ]

        $(this.table_holder)
            .empty()
            .append(t);

        this.table = $(t).DataTable({
            columns: columns,
            columnDefs: [
                {
                    targets: 0,
                    width: '60px',
                    orderable: false,
                    searchable: false,
                    render: function() {
                        return that._table_buttons();
                    }
                }
            ],
            drawCallback: () => this._bind_row_icons(),
            order: [[1, 'asc'],],
            lengthMenu: false,
            lengthChange: false,
            pageLength: 25
        });

        $.each(data, (i,v) => {
            this.add_table_row(v);
        });

        this.table.state.clear();
        this.table.draw();
    }

    add_table_row(v) {
        let data = ['', v.priority, v.name, v.description];
        let row = this.table.row.add(data);
        $(row.node()).data('base_data', v);
        return row;
    }

    load_table() {
        $.ajax({
            method: 'GET',
            url: '/api/category', 
            success:(rslt) => {
                this._render_table(rslt);
            },
        });
    }
}


class Category {
    constructor() {
        this.modal = $('#admin-modal');
        this.new_modal_created = false;

        //this.cat_table = new CategoryTable;

        this.params = {
            title: '<b>New Category</b>',
            submit_callback: this._submit_callback,
            submit_text: 'Submit',
            entries: [
                {
                    id: 'new-cat-name', 
                    type: 'input',
                    type_type: 'text',
                    title: '<b>Name</b> <span class="small">*i.e. "2017 Introductions" or "Weekly For Sale".</span>',
                    placeholder: 'name'
                },
                {
                    id: 'new-cat-desc', 
                    type: 'textarea',
                    title: '<b>Description</b> <span class="small">*private notes for admin reference.</span>',
                    //placeholder: 'name'
                },
                {
                    id: 'new-cat-priority', 
                    type: 'input',
                    type_type: 'number',
                    title: '<b>Priority</b> <span class="small">*order to appear on web page. unique. lower numbers appear first.</span>',
                    placeholder: '0'
                },
                {
                    id: 'new-cat-html', 
                    type: 'html',
                    title: '<b>Page Description</b> <span class="small">*description of category to appear on page</span>'
                }
            ],
        }
    };

    _get_modal_form() {
        return {
            name: $('#new-cat-name').val(),
            description: $('#new-cat-desc').val(),
            priority: $('#new-cat-priority').val(),
            html: tinymce.get('new-cat-html').getContent() 
        };
    };

    _new(modal) {
        let scope = this;

        $.ajax({
            method: 'POST',
            url: '/api/category', 
            data: this._get_modal_form(),
            success:(rslt) => {
                let row = window.category_table.add_table_row(rslt);
                row.draw();
                Amu.ab_disab(scope.mod_submit, false);
                scope.modal.modal('hide')
            },
            error:(e) => {
                let msg = '<b>Error saving changes: </b>' + e.responseJSON.message; 
                Amu.modal_alert(scope.modal, msg, ['alert-danger']);
                Amu.ab_disab(scope.mod_submit, false);
            }
        });
    };

    _update(modal, id, row) {
        let scope = this;

        $.ajax({
            method: 'PUT',
            url: '/api/category/' + id, 
            data: this._get_modal_form(),
            success:(rslt) => {
                row.remove().draw()
                let new_row = window.category_table.add_table_row(rslt);
                new_row.draw();
                Amu.ab_disab(scope.mod_submit, false);
                scope.modal.modal('hide')
            },
            error:(e) => {
                let msg = '<b>Error saving changes: </b>' + e.responseJSON.message; 
                Amu.modal_alert(scope.modal, msg, ['alert-danger']);
                Amu.ab_disab(scope.mod_submit, false);
            }
        });
    };

    input_modal(action, params, id, row) {

        this.modal = Amu.populate_modal(this.modal, params);
        this.modal.modal({
            backdrop: 'static'
        });
        this.mod_submit = $(this.modal).
                                find('.modal-footer .btn-primary');
        $(this.mod_submit)
            .unbind('click')
            .on('click', () => {
                console.log(action);
                Amu.ab_disab(this.mod_submit, true);
                if (action == 'edit') {
                    this._update(this.modal, id, row);
                }
                else {
                    this._new(this.modal);
                }
            })
    };

    edit_entry(row) {
        let data = $(row.node()).data().base_data;
        let params = window.category.params;

        params.entries[0].val = data.name; 
        params.entries[1].val = data.description;
        params.entries[2].val = data.priority; 
        params.entries[3].val = data.html;

        this.input_modal('edit', params, data.id, row);
    };

    add_new() {
        this.input_modal('new', this.params);
    };
}

(function() {
    window.category = new Category();
    window.category_table = new CategoryTable();

    category_table.load_table();

    $('#add-cat').click(function() {
        category.add_new();
    });

}());
