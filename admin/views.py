from flask import Blueprint, render_template, request, url_for, redirect, \
    send_from_directory, abort
from flask_login import logout_user, login_required
import json, time, os, csv

from io import StringIO

from PIL import Image

from app import app
from auth import user_auth 

from models import *

admin = Blueprint(
    'admin', 
    __name__, 
    template_folder='templates',
    static_folder='static'
    )


def image_resizes(fpath):
    fname = os.path.basename(fpath)
    s = fname.split('.')
    fbase = s[0]
    ext = s[1]
    fname_720 = fbase + '_720.' + ext 
    fname_360 = fbase + '_360.' + ext 
    fname_thumb = fbase + '_thumb.' + ext  
    path_720 = os.path.join(app.root_path, 
        'static', 'plant_photos', '720', fname_720)
    path_360 = os.path.join(app.root_path, 
        'static', 'plant_photos', '360', fname_360)
    tpath = os.path.join(app.root_path, 
        'static', 'plant_photos', 'thumb', fname_thumb)

    im = Image.open(fpath)

    resize_720 = im.copy() 
    resize_720.thumbnail((720, 720), Image.ANTIALIAS)
    resize_360 = im.copy() 
    resize_360.thumbnail((360, 360), Image.ANTIALIAS)
    thumb = im.copy()
    thumb.thumbnail((128, 128), Image.ANTIALIAS)

    resize_720.save(path_720)
    resize_360.save(path_360)
    thumb.save(tpath)

    return {
        'filename_fullsize': fpath,
        'filename_720': path_720,
        'filename_360': path_360,
        'url_fullsize': '/static/plant_photos/fullsize/' + fname,
        'url_360': '/static/plant_photos/360/' + fname_360,
        'url_720': '/static/plant_photos/720/' + fname_720,
        'filename_thumb': tpath,
        'url_thumb': '/static/plant_photos/thumb/' + fname_thumb
        }


@admin.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    if user_auth(request):
        dest = request.args.get('next')
        try:
            assert dest is not None
            return redirect(dest)
        except:
            return redirect('admin/plants')

    return redirect('admin/login')


@admin.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect('admin/login')


@admin.route('/upload_plant_photo', methods=['GET', 'POST']) 
def upload_plant_photo():
    f = request.files.get('file')
    if f is None or f.filename == '':
        abort(415)

    #TODO verify filetype
    ext = f.filename.split('.')[-1]
    t = str(int(time.time()))
    fname = t + '.' + ext 
    fpath = os.path.join(app.root_path, 'static', 'plant_photos', 'fullsize', fname)
    fwrite = open(fpath, 'wb')
    fwrite.write(f.read())

    paths = image_resizes(fpath)

    return json.dumps(paths)


@admin.route('/plants', methods=['GET'])
@login_required
def admin_plants():
    return render_template('plants.html')


@admin.route('/categories', methods=['GET'])
@login_required
def admin_categories():
    return render_template('categories.html')


@admin.route('/system', methods=['GET'])
@login_required
def system():
    return render_template('system.html')


@admin.route('/price_list_template', methods=['GET', 'POST'])
@login_required
def price_template():
    return send_from_directory('admin/static', 'docs/price_list_template.csv', as_attachment=True)


@admin.route('/upload_price_list', methods=['GET', 'POST'])
@login_required
def upload_price_list():

    if 'file' not in request.files:
        abort(415)

    file = request.files['file']

    if not file:
        abort(415)

    if file.filename == '':
        abort(415)

    csv_content = file.read().decode().splitlines()

    reader = csv.DictReader(csv_content) 

    for row in reader:
        cultivar = row.get('Cultivar')
        hybridizer = row.get('Hybridizer')
        year = row.get('Year')
        foliage = row.get('Foliage')
        ploidy = row.get('Ploidy')
        scape = row.get('Scape Inches')
        blossom = row.get('Blossom Inches')
        bud_count = row.get('Bud Count')
        branching = row.get('Branching')
        price = row.get('Price', 0)
        description = row.get('Description')
        count = row.get('Count Available', 0)


        html = '''<ul>
                    <li>Hybridizer: %s</li>
                    <li>Year: %s</li>
                    <li>Foliage: %s</li>
                    <li>Ploidy: %s</li>
                    <li>Scape Inches: %s</li>
                    <li>Blossom Inches: %s</li>
                    <li>Bud Count: %s</li>
                    <li>Branching: %s</li>
                  </ul>
                    ''' % (hybridizer, year, foliage, ploidy, scape, blossom,
                            bud_count, branching)

        try:
            plant = db.session.query(Cultivar).filter_by(name=cultivar).first()

            if plant is None:
                plant = Cultivar(name=cultivar, html=html) 

            db.session.add(plant)
            db.session.flush()

            sqp = db.session.query(SubItemQtyPrice) \
                .filter_by(cultivar_id=plant.id, item=description, 
                            qty_available=count, price=price).first()

            if sqp is None:
                desc_qty_price = SubItemQtyPrice(cultivar_id=plant.id, item=description, 
                               qty_available=count, price=price)

                db.session.add(desc_qty_price)

            db.session.commit()

        except Exception as e:
            abort(400, str(e))

    return json.dumps({'result': 'ok'}) 
