from flask_script import Command
from app import db
from models import ShippingRate, SaleStatus, User

'''create seed lookup data'''

class MVDSeed(Command):

    def _seed_shipping_rate(self):
        generic_zip = 'all'
        generic_area = 'all'
        generic_rate = 7.95

        new_rate = ShippingRate(
                        zip_=generic_zip,
                        area=generic_area,
                        rate=generic_rate
                    )
        db.session.add(new_rate)
        db.session.commit()


    def _seed_sales_status(self):
        sales_status = ['paid-unshipped', 'paid-shipped', 'paid-cancelled', 'paid-refunded', 'unpaid', 'returned-not-refunded', 'returned-refunded', 'other']
        for s in sales_status:
            status = SaleStatus(description=s)
            db.session.add(status)
        db.session.commit()


    #def _seed_admin(self):
        #p = raw_input('admin password? ')

        #u = User(
                #username='super_hemerocallis',
                #firstname='Day',
                #lastname='Lilly',
                #email='',
                #password=p,
            #)
        #db.session.add(u)
        #db.session.commit()


    def run(self):
        self._seed_shipping_rate()
        self._seed_sales_status()


class CreateAdmin(Command):
    def run(self):
        f = input('admin firstname? ')
        l = input('admin lastname? ')
        u = input('admin username? ')
        p = input('admin password? ')
        e = input('admin email? ')

        user = User(
                username=u,
                firstname=f,
                lastname=l,
                email=e,
                password=p,
            )
        db.session.add(user)
        db.session.commit()
