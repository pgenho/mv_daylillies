from flask_script import Command
from app import db
from models import *

class TestDBCleanup(Command):
    '''cleanup any hanging db entries from tests''' 
    def run(self):

        users = db.session.query(User).filter(
           User.username.like('xx_%') 
        ).all()
        for u in users:
            db.session.delete(u)

        categories = db.session.query(Category).filter(
           Category.name.like('xx_%') 
        ).all()
        for c in categories:
            db.session.delete(c)

        cultivars = db.session.query(Cultivar).filter(
           Cultivar.name.like('xx_%') 
        ).all()
        for c in cultivars:
            db.session.delete(c)

        customers = db.session.query(Customer).filter(
           Customer.username.like('xx_%') 
        ).all()
        for c in customers:
            db.session.delete(c)

        db.session.commit()
