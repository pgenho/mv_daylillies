#!/usr/bin/env python

from app import db

import unittest

from models import User, Customer, Category, Cultivar, Price, \
CountAvailable, Sale, PlantPhoto 


        
class TestUserModel(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        user = db.session.query(User) \
            .filter_by(username='xx_username') \
            .first()
        if user:
            db.session.delete(user)

        db.session.commit()

    def test_user(self):
        u = User(
            'xx_username',
            'xx_first', 
            'xx_last',
            'xx_email',
            'passme')
        db.session.add(u)
        db.session.commit()

        u = db.session.query(User) \
            .filter_by(username='xx_username') \
            .first()

        #passwords
        self.assertTrue(u.validate_password('passme'))
        self.assertFalse(u.validate_password('dontpassme'))

        #update
        u.email = 'testremove'
        db.session.commit()

        u = db.session.query(User) \
            .filter_by(username='xx_username') \
            .first()

        self.assertEqual(u.email, 'testremove')

        u.password = 'newpass'
        db.session.commit()
        u = db.session.query(User) \
            .filter_by(username='xx_username') \
            .first()

        self.assertTrue(u.validate_password('newpass'))

        #delete
        db.session.delete(u)
        db.session.commit()

        u = db.session.query(User) \
            .filter_by(username='xx_username') \
            .first()

        self.assertTrue(u is None)


class TestPlant(unittest.TestCase):
    #TODO add tests for count entry addition.

    def setUp(self):
        cat = Category()
        cat.name = 'xx_test_cat'
        cat.description = 'xx_cat_desc'
        cat.priority = 11123223238
        cat.html = '<html>'
        db.session.add(cat)

        cul = Cultivar()
        cul.name = 'xx_test_cul'
        cul.description = 'xx_cul_desc'
        cul.html = '<html>'
        db.session.add(cul)

        db.session.commit()

    def tearDown(self):
        c = db.session.query(Category) \
            .filter_by(name='xx_test_cat') \
            .first()
        db.session.delete(c)
        db.session.commit()

        c = db.session.query(Cultivar) \
            .filter_by(name='xx_test_cul') \
            .first()
        db.session.delete(c)
        db.session.commit()

        p = db.session.query(PlantPhoto) \
            .filter_by(title='xx_title') \
            .first()
        try:
            db.session.delete(p)
        except:
            pass
        db.session.commit()


    def _get_cat(self):
        c = db.session.query(Category) \
            .filter_by(name='xx_test_cat') \
            .first()
        return c

    
    def test_catagory(self):
        c = self._get_cat() 
        self.assertEqual(c.description, 'xx_cat_desc') 
        #update
        tu = c.time_updated
        c.html = '<div>'
        db.session.commit()
        self.assertTrue(c.time_updated > tu)


    def test_photo(self):
        c = self._get_cat() 

        cul = Cultivar()
        cul.name = 'xx_photo_cul'
        cul.category_id = c.id
        cul.description = 'xx_cul_desc'
        cul.html = '<img>'
        db.session.add(cul)
        db.session.commit()

        p = PlantPhoto() 
        p.cultivar_id = cul.id
        p.title = 'xx_title'
        p.description = 'xx_desc'
        p.filename = 'xxx'
        p.url = 'xx_url'
        db.session.add(p)
        db.session.commit()

        p = db.session.query(PlantPhoto) \
            .filter_by(title='xx_title') \
            .first()

        self.assertEqual(p.url, 'xx_url')

        c = db.session.query(Cultivar) \
            .filter_by(id=p.cultivar_id) \
            .first()

        d = c.to_dict()
        self.assertEqual(d['category']['name'], 'xx_test_cat')
        self.assertEqual(c.html, '<img>')

        #test delete cascade
        db.session.delete(c)
        db.session.commit()

        self.assertIsNone(
            db.session.query(PlantPhoto) \
                .filter_by(title='xx_title') \
                .first()
        )




if __name__ == '__main__':
    unittest.main()
