#!/usr/bin/env python

from app import db

import unittest
import requests
import subprocess
import time
import os

base_url = 'http://localhost:5000/'


class TestUser(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_user(self):
        #insert
        data = {
            'username': 'xx_test_name',
            'firstname': 'xx_test_first',
            'lastname': 'xx_test_last',
            'email': 'xx_test_email',
            'password': 'xx_test_pass',
        }
        url = base_url + 'api/add_user' 
        r = requests.post(url, data=data)
        self.assertTrue(r.status_code == 200)
        id = r.json()['id']
        
        #retrieve
        url = base_url + 'api/user/%s' % id 
        r = requests.get(url)
        username = r.json()['username']
        self.assertEqual(username, data['username'])
        
        #update
        data = {'username': 'xx_test_name_altered'}
        r = requests.put(url, data=data) 
        url = base_url + 'api/user/%s' % id 
        r = requests.get(url)
        username = r.json()['username']
        self.assertEqual(username, 'xx_test_name_altered')


class TestPlants(unittest.TestCase):
    
    def _create_category(self):
        url = base_url + 'api/category'
        data = {
            'name': 'xx_test_category',
            'description': 'xx_category_desc',
            'priority': 112112,
            'html': '<div>' 
        }
        r = requests.post(url, data=data)
        try:
            return r.json()
        except: 
            return


    def test_category(self):
        #create
        c = self._create_category()
        self.assertTrue(c is not None)
        self.assertEqual(c['name'], 'xx_test_category')

        #get all categories, check recent insert is there
        url = base_url + 'api/category'
        try:
            r = requests.get(url).json()
        except Exception as e:
            raise e
        found = False
        for i in r: 
            if i['name'] == 'xx_test_category':
                found = True            
        self.assertTrue(found)

        #update
        url = base_url + 'plants/category/%s' % c['id']
        data = {
            'description': 'xx_new_desc'
        }
        r = requests.put(url, data=data)
