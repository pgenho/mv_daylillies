from app import db
import datetime
import time
from flask_login import UserMixin

from passlib.hash import bcrypt


def encrypt_update(target, value, oldvalue, initiator):
    '''hash passwords on update rather than store plaintext'''
    return bcrypt.hash(value)


def serialize_datetimes(dct):
    #TODO just don't. but for now we will.
    for k in dct.keys():
        if isinstance(dct[k], (datetime.datetime, datetime.date)):
            dct[k] = dct[k].strftime('%x %I:%M %p')
    return dct


class User(db.Model, UserMixin):
    '''For system users.'''
    __tablename__ = 'user'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        del dct['password']
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Unicode, nullable=False, unique=True)
    firstname = db.Column(db.Unicode, nullable=False)
    lastname = db.Column(db.Unicode, nullable=False)
    email = db.Column(db.String, nullable=True)
    password = db.Column(db.String(128), nullable=False)
    last_login = db.Column(db.DateTime, default=datetime.datetime.now)
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)

    def __init__(self, username, firstname, lastname, email, password):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password 

    def validate_password(self, password):
        return bcrypt.verify(password, self.password)


#cultivar_categories = db.Table('cultivar_categories',
    #db.Column('category_id', db.Integer, db.ForeignKey('category.id'), primary_key=True),
    #db.Column('cultivar_id', db.Integer, db.ForeignKey('cultivar.id'), primary_key=True)
    #)

class CultivarCategories(db.Model):
    __tablename__ = 'cultivar_category'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    category_id = db.Column('category_id', db.Integer, db.ForeignKey('category.id'))
    cultivar_id = db.Column('cultivar_id', db.Integer, db.ForeignKey('cultivar.id'))

    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)

    __table_args__ = (db.UniqueConstraint('cultivar_id', 'category_id'),)

#cultivar_subqtyprice = db.Table('cultivar_subqtyprice',
    #db.Column('category_id', db.Integer, db.ForeignKey('category.id'), primary_key=True),
    ##db.Column('subitem_qty_price', db.Integer, db.ForeignKey('subitem_qty_price.id'), primary_key=True)
    #)


class Category(db.Model):
    __tablename__ = 'category'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(720), unique=True, index=True)
    description = db.Column(db.String(2000))
    priority = db.Column(db.Integer, unique=True, nullable=False)
    html = db.Column(db.Text())
    cultivars = db.relationship('CultivarCategories', 
                                    #secondary=cultivar_categories, 
                                    lazy='subquery', 
                                    backref=db.backref('category', lazy=True))
    #cultivars = db.relationship('Cultivar')
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)


class Cultivar(db.Model):
    __tablename__ = 'cultivar'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        dct['categories'] = [i.to_dict() for i in self.category] 
        dct['subitem_count_prices'] = [i.to_dict() for i in self.subitem_qty_price] 
        dct['photos'] = [i.to_dict() for i in self.photos] 
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(720), unique=True, index=True)
    description = db.Column(db.String(2000))
    html = db.Column(db.Text())
    show_on_site = db.Column(db.Boolean, default=True) 
    show_on_frontpage = db.Column(db.Boolean, default=False) 
    #price_id = db.Column(db.Integer, db.ForeignKey('price.id'))

    hybridizer = db.Column(db.Text())
    year_introduced = db.Column(db.Text())
    foliage_type = db.Column(db.Text())
    ploidy = db.Column(db.Text())
    scape_height = db.Column(db.Text())
    blossom_size = db.Column(db.Text())
    bud_count = db.Column(db.Text())
    branching = db.Column(db.Text())

    #categories = db.relationship('Category', 
                                    #secondary=cultivar_categories, 
                                    #lazy='subquery', 
                                    #backref=db.backref('cultivar', lazy=True))

    category = db.relationship('CultivarCategories', 
                                    lazy='joined', 
                                    backref=db.backref('cultivar_category', lazy=True))

    subitem_qty_price = db.relationship('SubItemQtyPrice', 
                                    lazy='joined', 
                                    single_parent=True,
                                    backref=db.backref('cultivar_subqtyprice', lazy=True))

    #price = db.relationship(
                    #'Price', 
                    #foreign_keys=[price_id], 
                    #backref='cultivar', 
                    #lazy=True)

    photos = db.relationship('PlantPhoto', 
        lazy='select',
        backref=db.backref('cultivar_photos', 
            lazy='joined', 
            single_parent=True,
            cascade="all, delete-orphan"
            )
        )
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)


class FeaturedPhotos(db.Model):
    __tablename__ = 'featured_photos'

    id = db.Column(db.Integer, primary_key=True)
    cultivar_id = db.Column(db.Integer, 
                        db.ForeignKey('cultivar.id'), 
                        unique=True,
                        index=True
                        )
    photo_id = db.Column(db.Integer, 
                        db.ForeignKey('plant_photo.id'), 
                        index=True
                        )

    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)


class PlantPhoto(db.Model):
    __tablename__ = 'plant_photo'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        is_main = db.session.query(FeaturedPhotos) \
                    .filter_by(cultivar_id=self.cultivar_id, photo_id=self.id) \
                    .first()

        if is_main:
            dct['is_featured'] = True
        else:
            dct['is_featured'] = False

        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)
    cultivar_id = db.Column(db.Integer, db.ForeignKey('cultivar.id'), nullable=True)
    title = db.Column(db.Unicode())
    description = db.Column(db.Unicode())
    filename_fullsize = db.Column(db.String(200))
    url_fullsize = db.Column(db.String(200))
    filename_720 = db.Column(db.String(200))
    url_720 = db.Column(db.String(200))
    filename_360 = db.Column(db.String(200))
    url_360 = db.Column(db.String(200))
    filename_thumb = db.Column(db.String(200))
    url_thumb = db.Column(db.String(200))
    #is_featured = db.Column(db.Boolean)
    cultivar = db.relationship('Cultivar',
        foreign_keys=[cultivar_id], 
        lazy='select',
        backref=db.backref('plant_photo', 
            lazy='joined', cascade="all, delete-orphan"
            )
        )
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)


class SubItemQtyPrice(db.Model):
    __tablename__ = 'subitem_qty_price'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)

    cultivar_id = db.Column(db.Integer, db.ForeignKey('cultivar.id'))

    item = db.Column(db.String(720), index=True)
    qty_available = db.Column(db.Integer, default=0)
    price = db.Column(db.Float, default=0.0)

    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)

    __table_args__ = (db.UniqueConstraint('cultivar_id', 'item'),)


#class CountAvailable(db.Model):
    #__tablename__ = 'count_available'

    #id = db.Column(db.Integer, primary_key=True)
    ##reference price to get specific item (i.e 1Fan, 2Fans etc).
    #price_id = db.Column(db.Integer, db.ForeignKey('price.id'))
    #count = db.Column(db.Integer, default=0)
    #price = db.relationship('Price',
        #foreign_keys=[price_id], 
        #lazy='select',
        #backref=db.backref('available_count', 
            #lazy='joined', cascade="all, delete-orphan"
            #)
        #)
    #time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    #time_updated = db.Column(db.DateTime, 
        #default=datetime.datetime.now, onupdate=datetime.datetime.now)


#class Price(db.Model):
    #__tablename__ = 'price'

    #id = db.Column(db.Integer, primary_key=True)
    #cultivar_id = db.Column(db.Integer, db.ForeignKey('cultivar.id'), nullable=False)
    #description = db.Column(db.String)
    #price = db.Column(db.Float)
    ##cultivar = db.relationship('Cultivar',
        ##foreign_keys=[cultivar_id], 
        ##lazy='select',
        ##backref=db.backref('plant_price', 
            ##lazy='joined', cascade="all, delete-orphan"
            ##)
        ##)

    #__table_args__ = (db.UniqueConstraint(cultivar_id, description), )

    #time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    #time_updated = db.Column(db.DateTime, 
        #default=datetime.datetime.now, onupdate=datetime.datetime.now)


class Customer(db.Model):
    __tablename__ = 'customer'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        del dct['password']
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.Unicode, nullable=False, unique=True, index=True)
    firstname = db.Column(db.Unicode, nullable=False)
    lastname = db.Column(db.Unicode, nullable=False)

    address1 = db.Column(db.String(256), nullable=False)
    address2 = db.Column(db.String(256), nullable=True)
    city = db.Column(db.String(256), nullable=False)
    state = db.Column(db.String(256), nullable=False)
    zip_ = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(256), nullable=False)
    phone = db.Column(db.String(256), nullable=True)

    password = db.Column(db.String(128), nullable=False)

    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, 
        default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def __init__(self, username, firstname, lastname, email, password):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.password = password 

    def validate_password(self, password):
        return bcrypt.verify(password, self.password)


class SaleSource(db.Model):
    __tablename__ = 'sale_source'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(2000))
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)


class Sale(db.Model):
    __tablename__ = 'sale'
    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        items = db.query(SaleItem).filter(sale_id=self.id).all()
        dct['items'] = [i.to_dict() for i in items]
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)
    sale_date = db.Column(db.DateTime, default=datetime.datetime.now)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'))
    cultivar_id = db.Column(db.Integer, db.ForeignKey('cultivar.id'))
    discount = db.Column(db.Float)
    tax = db.Column(db.Float)
    shipping_rate = db.Column(db.Integer, db.ForeignKey('shipping_rate.id'))
    status_id = db.Column(db.Integer, db.ForeignKey('sale_status.id'))
    status = db.relationship('SaleStatus',
        foreign_keys=[status_id], 
        lazy='select',
        backref=db.backref('sale_status', 
            lazy='joined', cascade="all, delete-orphan"
            )
        )
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)


class SaleItem(db.Model):
    __tablename__ = 'sale_item'

    def to_dict(self):
        dct = {i.name: getattr(self, i.name) for i in self.__table__.columns}
        return serialize_datetimes(dct)

    id = db.Column(db.Integer, primary_key=True)
    sale_id = db.Column(db.DateTime, default=datetime.datetime.now)
    qty = db.Column(db.Float)
    price = db.Column(db.Float)
    status_id = db.Column(db.Integer, db.ForeignKey('sale_status.id'))
    status = db.relationship('SaleStatus',
        foreign_keys=[status_id], 
        lazy='select',
        backref=db.backref('sale_item_status', 
            lazy='joined', cascade="all, delete-orphan"
            )
        )
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)


class SaleStatus(db.Model):
    __tablename__ = 'sale_status'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(200), nullable=False, unique=True)
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)


class ShippingRate(db.Model):
    __tablename__ = 'shipping_rate'

    id = db.Column(db.Integer, primary_key=True)
    zip_ = db.Column(db.String(256), nullable=False, unique=True)
    area = db.Column(db.String(256), nullable=True)
    rate = db.Column(db.Float)
    time_added = db.Column(db.DateTime, default=datetime.datetime.now)
    time_updated = db.Column(db.DateTime, onupdate=datetime.datetime.now)


#listen for password updates and hash
db.event.listen(User.password, 'set', encrypt_update, retval=True)
