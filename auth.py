from app import app, db
import flask_login
import json
from models import User


app.secret_key = 'asdasfd'


login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'admin.login'


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(id=user_id).first()


def user_auth(request):

    username = request.form.get('username') 
    passwd = request.form.get('password')
    remember = request.form.get('remember')

    db_user = db.session.query(User) \
        .filter_by(username=username) \
        .first()

    if db_user is None or not db_user.validate_password(passwd):
        return

    if remember:
        flask_login.login_user(db_user, remember=True)
    else:
        flask_login.login_user(db_user)

    return db_user
