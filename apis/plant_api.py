import os
import json
from datetime import datetime
from operator import itemgetter
from flask import request
from datetime import datetime, timedelta
from flask_restful import Resource, reqparse, abort

from app import api, db

from models import * 

from .api_utilities import update_attribs 

catparser = reqparse.RequestParser()
catparser.add_argument('name')
catparser.add_argument('description')
catparser.add_argument('html')
catparser.add_argument('priority')

photoparse = reqparse.RequestParser()
photoparse.add_argument('title')
photoparse.add_argument('description')
photoparse.add_argument('photo_info')

plant_parse = reqparse.RequestParser()
plant_parse.add_argument('data')


def _tmstr(timestamp):
    #TODO look at if timestamp or string 
    return timestamp
    #t = datetime.fromtimestamp(timestamp)
    #return t.strftime('%x %I:%M %p')


class LatestAdditions(Resource):
    def get(self):
        '''same number as number of categories'''
        count = db.session.query(Category).count()

        latest = db.session.query(Cultivar) \
                    .order_by(Cultivar.time_added) \
                    .limit(count) \

        rslt = [i.to_dict() for i in latest]
        return rslt


#class Specials(Resource):
    #def get(self):
        #specials = db.session.query(Cultivar) \
                        #.filter(Cultivar.category_id == Category.id) \
                        #.filter(Category.name == 'Specials') \
                        #.order_by(Category.name) \
                        #.all()
        
        #rslt = [i.to_dict() for i in specials]
        #return rslt


class Categories(Resource):
    def get(self):
        '''info for all categories'''
        cats = db.session.query(Category) \
            .order_by(Category.priority) \
            .all()
        rslt = [i.to_dict() for i in cats]
        return rslt

    #@classmethod
    #def all(cls):
        #return cls.get(cls)

    def post(self):
        '''add category'''
        try:
            args = catparser.parse_args()
            cat = Category(
                name=args['name'],
                description=args['description'],
                priority=args['priority'],
                html=args['html']
                )
            db.session.add(cat)
            db.session.commit()

            return cat.to_dict()

        except Exception as e:
            message = getattr(e, 'message', repr(e))
            abort(410, message=message)


class CategoryMod(Resource):
    def get(self, cat_id):
        return Category(id=cat_id).to_dict()

    def put(self, cat_id):
        '''update category'''
        args = catparser.parse_args()
        cat = db.session.query(Category).filter_by(id=cat_id).first()
        if cat is not None:
           update_attribs(cat, request) 
           db.session.commit()
        return cat.to_dict()

    def delete(self, cat_id):
        '''delete category'''
        cat = db.session.query(Category).filter_by(id=cat_id).first()
        db.session.delete(cat)
        db.session.commit()


class CategoryWithPlants(Resource):
    def get(self, cat_name):

        cat = db.session.query(Category) \
                .filter_by(name=cat_name) \
                .first()

        if cat is None:
            abort(404, message='Not Found')

        plants = db.session.query(Cultivar).join(CultivarCategories). \
                    filter(Cultivar.id==CultivarCategories.cultivar_id). \
                    filter(CultivarCategories.category_id==cat.id)

        return  {
            'category': cat.to_dict(),
            'plants': [i.to_dict() for i in plants]
        }


class Plant(Resource):
    def get(self, cult_id):
        #'''cultivar info for single'''
        rslt = db.session.query(Cultivar) \
                .filter_by(id=cult_id) \
                .first()

        if rslt is None:
            return {}

        return rslt.to_dict()


    def put(self, cult_id):
        #'''update cultivar'''
        args = plant_parse.parse_args()
        data = args['data']
        data = json.loads(data)

        plant = db.session.query(Cultivar).filter_by(id=cult_id).first()

        if plant is None:
            return {}

        plant.name = data['name']
        plant.html = data['html']
        plant.show_on_site = data.get('show_on_site', False)
        plant.show_on_frontpage = data.get('show_on_frontpage', False)

        if 'subitem_count_price' in data:
            subitem_count_price = data['subitem_count_price']
        else:
            subitem_count_price = []

        cults = db.session.query(SubItemQtyPrice) \
            .filter_by(cultivar_id=cult_id) \
            .all()

        for c in cults:
            db.session.delete(c)

        db.session.commit()

        for subqty in subitem_count_price:
            sqp = SubItemQtyPrice(
                cultivar_id=plant.id,
                item = subqty[0], 
                qty_available = subqty[1], 
                price = subqty[2], 
            )
            try:
                db.session.add(sqp)
                db.session.flush()
            except Exception as e:
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)


        categories = data.get('categories', []) 

        cats = db.session.query(CultivarCategories) \
            .filter_by(cultivar_id=plant.id) \
            .all()

        for c in cats:
            db.session.delete(c)

        db.session.commit()
        db.session.flush()

        cats = db.session.query(CultivarCategories) \
            .filter_by(cultivar_id=plant.id) \
            .all()

        #print(categories)

        for cat in categories:
            try:
                cat = int(cat)
            except Exception as e:
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)

            cc = CultivarCategories(
                category_id=cat,
                cultivar_id=plant.id,
            )
            try:
                db.session.add(cc)
                db.session.flush()
            except Exception as e:
                print('here', e)
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)


        photos = data.get('photos', []) 
        for photo in photos:

            file_data = photo.get('file_data')
            
            is_main = photo.get('main_photo', False)

            existing_photo = db.session.query(PlantPhoto) \
                .filter_by(
                    filename_fullsize=file_data['filename_fullsize'],
                    cultivar_id=plant.id
                    ) \
                .first()

            if existing_photo is None:
                existing_photo = PlantPhoto(
                    cultivar_id=plant.id,
                    title=photo['title'],
                    description=photo['description'],
                    filename_fullsize=file_data['filename_fullsize'], 
                    url_fullsize=file_data['url_fullsize'],
                    filename_720=file_data['filename_720'],
                    url_720=file_data['url_720'],
                    filename_360=file_data['filename_360'],
                    url_360=file_data['url_360'],
                    filename_thumb=file_data['filename_thumb'],
                    url_thumb=file_data['url_thumb'],
                )
                db.session.add(existing_photo)
                try:
                    db.session.flush()
                except Exception as e:
                    message = getattr(e, 'message', repr(e))
                    abort(410, message=message)
            else:
                existing_photo.title = photo.get('title', '')
                existing_photo.description = photo.get('description', '')
                existing_photo.is_featured = is_main 

            if is_main:
                featured = db.session.query(FeaturedPhotos) \
                                .filter_by(cultivar_id=plant.id) \
                                .first()
                if featured:
                    featured.photo_id = existing_photo.id
                else:
                    featured = FeaturedPhotos(cultivar_id=plant.id, photo_id=existing_photo.id)
                db.session.add(featured)

        try:
            db.session.commit()
        except Exception as e:
            message = getattr(e, 'message', repr(e))
            abort(410, message=message)

        plant_photos = db.session.query(PlantPhoto) \
                    .filter_by(cultivar_id=plant.id) \
                    .order_by('time_added') \
                    .all()

        out = plant.to_dict()

        out['photo_count'] = len(photos)
        out['time_added'] = _tmstr(out['time_added'])
        out['time_updated'] = _tmstr(out['time_updated'])
        out['photos'] = [i.to_dict() for i in plant_photos]

        return out


    def delete(self, cult_id):
        #'''delete cultivar'''
        cult = db.session.query(Cultivar).filter_by(id=cult_id).first()
        db.session.delete(cult)
        db.session.commit()
        #TODO delete photos


class Plants(Resource):
    def get(self):
        '''all plants info''' 
        out = []
        plnts = db.session.query(Cultivar) \
                    .order_by(Cultivar.name) \
                    .all()
        for p in plnts:
            tmp = p.to_dict()

            tmp['time_added'] = _tmstr(tmp['time_added'])
            tmp['time_updated'] = _tmstr(tmp['time_updated'])

            sql = '''SELECT COUNT(*)
                        FROM plant_photo
                        WHERE cultivar_id=:pid''' 

            photo_count = db.session.execute(sql, {'pid':p.id}).first()

            photos = db.session.query(PlantPhoto) \
                        .filter_by(cultivar_id=p.id) \
                        .order_by('time_added') \
                        .all()

            if photo_count and len(photo_count) > 0:
                tmp['photo_count'] = photo_count[0]
            else:
                tmp['photo_count'] = 0 

            tmp['photos'] = [i.to_dict() for i in photos]

            out.append(tmp)
        return out 

    def post(self):
        '''new cultivar'''
        args = plant_parse.parse_args()
        data = args['data']
        data = json.loads(data)

        plant = Cultivar(
            name=data['name'],
            description='',
            #category_id=int(data['category']),
            html=data['html'],
            #count_available=int(data.get('count_available', 0)),
            #price=int(data.get('count_available', 0)),
            show_on_site=data.get('show_on_site', False),
            show_on_frontpage=data.get('show_on_frontpage', False),
            )

        try:
            db.session.add(plant)
            db.session.flush()
        except Exception as e:
            message = getattr(e, 'message', repr(e))
            abort(410, message=message)

        if 'subitem_count_price' in data:
            subitem_count_price = data['subitem_count_price']
        else:
            subitem_count_price = []

        categories = data.get('categories', []) 

        #sub quantity price
        for subqty in subitem_count_price:

            sqp = SubItemQtyPrice(
                cultivar_id=plant.id,
                item = subqty[0], 
                qty_available = subqty[1], 
                price = subqty[2], 
            )
            try:
                db.session.add(sqp)
                db.session.flush()
            except Exception as e:
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)


        for cat in categories:
            try:
                cat = int(cat)
            except Exception as e:
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)

            cc = CultivarCategories(
                category_id=cat,
                cultivar_id=plant.id,
            )
            try:
                db.session.add(cc)
                db.session.flush()
            except Exception as e:
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)


        photos = data.get('photos', []) 
        for photo in photos:
            file_data = photo.get('file_data')

            is_main = photo.get('main_photo')

            entry = PlantPhoto(
                cultivar_id=plant.id,
                title=photo['title'],
                description=photo['description'],
                filename_fullsize=file_data['filename_fullsize'], 
                url_fullsize=file_data['url_fullsize'],
                filename_720=file_data['filename_720'],
                url_720=file_data['url_720'],
                filename_360=file_data['filename_360'],
                url_360=file_data['url_360'],
                filename_thumb=file_data['filename_thumb'],
                url_thumb=file_data['url_thumb'],
            )

            try:
                db.session.add(entry)
                db.session.flush()
            except Exception as e:
                message = getattr(e, 'message', repr(e))
                abort(410, message=message)

            photo_id = entry.id 

            #unique photo
            if is_main:
                main_photo = db.session.query(FeaturedPhotos) \
                                .filter_by(cultivar_id=plant.id) \
                                .first()
                if not main_photo:
                    main_photo = FeaturedPhotos(cultivar_id=plant.id, photo_id=photo_id)
                    db.session.add(main_photo)
                else:
                    main_photo.photo_id = photo_id

        db.session.commit()

        plant_photos = db.session.query(PlantPhoto) \
                    .filter_by(cultivar_id=plant.id) \
                    .order_by('time_added') \
                    .all()

        out = plant.to_dict()
        out['photo_count'] = len(photos)
        out['time_added'] = _tmstr(out['time_added'])
        out['time_updated'] = _tmstr(out['time_updated'])
        out['photos'] = [i.to_dict() for i in plant_photos]

        return out


class Photos(Resource):
    def get(self, cult_id):
        '''get list of photos'''
        photos = db.session.query(PlantPhoto).filter_by(id=cult_id).all()
        return [i.to_dict() for i in photos]

    def post(self, cult_id):
        '''uploaded photo. store file in filesystem and info in db.'''
        file = request.files['file']
        args = photoparse.parse_args()
        title = args['title']
        desc = args['description']
        filename = secure_filename(title.lower().replace(' ', '-'))
        filepath = os.path.join('static/photos/plant_photos', filename)
        #save file
        try:
            file.save(filename)        
        #TODO specify error.
        except Exception as e:
            return {'result': 'fail', 'message': e} 

        #info to db
        photo = PlantPhoto(
            cultivar_id=cult_id,
            title=title,
            description=desc,
            filename=filepath,
            url='/cultivar_photos/' + filename 
        )
        db.session.add(photo)
        db.session.commit()
        return {'result': 'ok'} 


class UpdatePhoto(Resource):
    def put(self, photo_id):
        '''update photo info in db. does not change underlying file.'''
        photo = db.session.query(PlantPhoto).filter_by(id=photo_id).first()
        args = photoparse.parse_args()
        photo.title = args['title']
        photo.description = args['description']
        db.session.add(photo)
        db.session.commit()
        return {'result': 'ok'}
        

    def delete(self, photo_id):
        '''remove photo info and delete photo file.
        a legit photo_id may not be passed in (i.e is uploaded but no db ref yet).
        '''

        form = request.form
        paths = [
                    form.get('path_360'),
                    form.get('path_720'),
                    form.get('path_full'),
                    form.get('path_thumb')
                    ]

        photo = db.session.query(PlantPhoto).filter_by(id=photo_id).first()

        if photo is not None:
            #remove reference
            db.session.delete(photo)
            db.session.commit()
            return {'result': 'ok'}
        else:
            #remove file
            for p in paths:
                if p is not None:
                    os.remove(p)


class FrontPage(Resource):
    def get(self):
        out = []
        plnts = db.session.query(Cultivar) \
                    .filter(Cultivar.show_on_frontpage==True) \
                    .order_by(Cultivar.name) \
                    .all()
        for p in plnts:
            tmp = p.to_dict()

            #tmp['time_added'] = _tmstr(tmp['time_added'])
            #tmp['time_updated'] = _tmstr(tmp['time_updated'])

            #price = db.session.query(Price) \
                        #.filter_by(cultivar_id=p.id) \
                        #.first()
            #if price:
                #tmp['price'] = price.price 
            #else:
                #tmp['price'] = 0 

            #count = db.session.query(CountAvailable) \
                        #.filter_by(cultivar_id=p.id) \
                        #.first()
            #if count:
                #tmp['count'] = count.count 
            #else:
                #tmp['count'] = 0 

            #sql = '''SELECT COUNT(*)
                        #FROM plant_photo
                        #WHERE cultivar_id=:pid''' 

            #photo_count = db.session.execute(sql, {'pid':p.id}).first()

            photos = db.session.query(PlantPhoto) \
                        .filter_by(cultivar_id=p.id) \
                        .join(FeaturedPhotos) \
                        .order_by('time_added') \
                        .all()

            #if photo_count and len(photo_count) > 0:
                #tmp['photo_count'] = photo_count[0]
            #else:
                #tmp['photo_count'] = 0 

            tmp['photos'] = [i.to_dict() for i in photos]

            out.append(tmp)
        return out 

    


api.add_resource(UpdatePhoto, '/api/update_photo/<string:photo_id>')
api.add_resource(Photos, '/api/photo/<string:cult_id>')
api.add_resource(Categories, '/api/category')
api.add_resource(CategoryMod, '/api/category/<string:cat_id>')
api.add_resource(Plant, '/api/plant/<string:cult_id>')
api.add_resource(Plants, '/api/plants')
api.add_resource(FrontPage, '/api/front_page')
api.add_resource(LatestAdditions, '/api/latest_addtions')
