'''misc api utilities'''


def update_attribs(obj, request):
    '''update object from values in request.
        :param obj: DB object
        :param request: http request (PUT generally). will obtain dict from.
    '''
    for k,v in request.values.items():
        if hasattr(obj, k):
            setattr(obj, k, v)
