import json
from flask import request
from datetime import datetime, timedelta
from flask_restful import Resource, reqparse, abort

from app import api, db

from models import User, Customer
from .api_utilities import update_attribs

parser = reqparse.RequestParser()

parser.add_argument('username')
parser.add_argument('firstname')
parser.add_argument('lastname')
parser.add_argument('email')
parser.add_argument('password')
parser.add_argument('address1')
parser.add_argument('address2')
parser.add_argument('city')
parser.add_argument('state')
parser.add_argument('zip')
parser.add_argument('phone')



class NewUser(Resource):
    def post(self):
        '''add user'''
        args = parser.parse_args()
        user = User(
            username=args['username'],
            firstname=args['firstname'],
            lastname=args['lastname'],
            email=args['email'],
            password=args['password']
            )
        db.session.add(user)
        db.session.commit()

        return user.to_dict();


class AllUsers(Resource):
    def get(self):
        '''all users'''
        users = db.session.query(User).all()
        return [i.to_dict() for i in users]


class SysUser(Resource):
    def get(self, user_id):
        '''get info'''
        return db.session.query(User) \
            .filter_by(id=user_id).first() \
            .to_dict()

    def put(self, user_id):
        '''update user'''
        user = db.session.query(User).filter_by(id=user_id).first()
        if user is not None:
           update_attribs(user, request) 
           db.session.commit()
        return user.to_dict()


    def delete(self, user_id):
        '''delete user'''
        #make sure to keep at least one user
        users = db.session.query(User).all()
        if len(users) < 2:
            return {'error': 'Error: Must have at least one admin user'}

        user = db.session.query(User).filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()


class CustomerAdd(Resource):
    def post(self):
        '''add customer'''
        args = parser.parse_args()
        user = Customer(
            username=args['username'],
            firstname=args['firstname'],
            lastname=args['lastname'],
            email=args['email'],
            address1=args['address1'],
            address2=args['address2'],
            city=args['city'],
            state=args['state'],
            zip_=args['zip'],
            phone=args['phone'],
            password=args['password']
            )
        db.session.add(user)
        db.session.commit()

        return user.to_dict();


class CustomerMod(Resource):
    def get(self, user_id):
        '''get info'''
        return Customer(id=user_id) 

    def put(self, user_id):
        '''update customer'''
        user = db.session.query(Customer).filter_by(id=user_id).first()
        if user is not None:
           update_attribs(user, request) 
           db.session.commit()

    def delete(self, user_id):
        '''delete user'''
        user = db.session.query(Customer).filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()


class AllCustomers(Resource):
    '''show info for users. accept get and post with no mods'''
    def show_all(self):
        users = Customer.query.order_by(customer.lastname).all() 
        return [i.to_dict() for i in users] 
    
    def get(self):
        return self.show_all()

    def post(self):
        return self.show_all()


api.add_resource(NewUser, '/api/add_user')
api.add_resource(AllUsers, '/api/users')
api.add_resource(SysUser, '/api/user/<string:user_id>')
api.add_resource(AllCustomers, '/api/all')
api.add_resource(CustomerAdd, '/api/add_customer')
api.add_resource(CustomerMod, '/api/<string:user_id>')
