import json
import os
from flask import request
from datetime import datetime, timedelta
from flask_restful import Resource, reqparse, abort

from app import api, db

from models import Cultivar, Price, Sale, Customer, CountAvailable 
from .api_utilities import update_attribs 


class AvailableCount(Resource):

    def get(self, cult_id):
        '''count available, "all" for cult_id gets for all plants'''
        pass

    def put(self, cult_id):
        '''update count available'''
        pass
    




api.add_resource(AvailableCount, '/plants/count/<string:cult_id>')
