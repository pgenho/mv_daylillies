from flask_restful import Resource, abort
import re

from app import api, db

def subword(txt, wrd):
    #txt = txt.replace('\n', ' ').replace('\t', ' ')
    txt = re.sub('<.*?>', ' ', txt)
    txt = re.sub('[^0-9a-zA-Z]+', ' ', txt)

    indx = False
    txt_lst = txt.split()

    for i in range(len(txt_lst)):
        text_word = txt_lst[i]
        if wrd.lower() in text_word.lower():
            indx = i
            break

    if indx:
        txt_lst[indx] = '<b>%s</b>' % txt_lst[indx] 
    
    strt = max(0, i-2)

    out = ' '.join(txt_lst[strt: strt + 6])

    return out


class SiteSearch(Resource):

    def get(self, word):
        engine = db.engine
        con = engine.connect()
        wrd = '%' + word + '%'

        sql = '''
           select id, name, description, html
           from cultivar
           where name like ? collate nocase or
           description like ? collate nocase or
           html like ? collate nocase
           order by name
        '''

        rslts = con.execute(sql, (wrd, wrd, wrd))

        if rslts is not None:
            rslts = [
                {
                    'id': i['id'],
                    'name': i['name'],
                    'text': re.sub(
                                '\s+',
                                ' ',
                                subword(';'.join([str(k) for k in i[1:]]), word)
                                )
                }
                for i in rslts]

        else:
            rslts = []

        con.close()

        return rslts 

        

api.add_resource(SiteSearch, '/api/site_search/<string:word>')
