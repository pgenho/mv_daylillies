#from flask import Flask
from flask_assets import Environment, Bundle

from app import app

assets = Environment(app)
assets.init_app(app)

vendor_css = Bundle(
    'site/node_modules/bootstrap/dist/css/bootstrap.css',
    output='css_min/vendor.css'
    )

assets.register('vendor_css', vendor_css)
