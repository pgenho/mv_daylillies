import os

#configuration options for setting app
#most of these will come from environmental variables but can hardcode in.
#to run the app locally set these values in local environment or hardcode 


class Config(object):
    '''development variables'''
    env = os.environ
    DEBUG = env.get('FLASK_DEBUG', False)
    ASSETS_DEBUG = False
    #default to sqlite database unless another is configured in env.
    SQLALCHEMY_DATABASE_URI = env.get('DATABASE_URL', 'sqlite:///data/mvd.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    #reload dev server on change
    TEMPLATES_AUTO_RELOAD = True
