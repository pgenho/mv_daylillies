from flask import Blueprint, render_template, request, url_for, redirect, current_app
from flask_login import logout_user, login_required

from apis.plant_api import Categories, FrontPage, LatestAdditions, Plant, \
    CategoryWithPlants


from app import app
from auth import user_auth 

import os

site = Blueprint(
    'site',
    __name__,
    template_folder='templates',
    static_folder='static'
    )


@site.route('/', methods=['GET', 'POST'])
@login_required
def site_home():
    cats = Categories()
    categories = cats.get()

    l = LatestAdditions()
    latest = l.get()

    fp = FrontPage()
    featureds = fp.get()

    return render_template(
        'site/home.html', 
        categories=categories,
        latest=latest,
        featureds=featureds
        )


@site.route('/plants/<name>/<id>', methods=['GET', 'POST'])
@login_required
def plant(name, id):

    p = Plant()
    info = p.get(id)

    return render_template(
        'site/plants.html', 
        name=name,
        info=info
        )


@site.route('/shop', methods=['GET', 'POST'])
@login_required
def shop():
    return render_template(
        'site/shop.html'
        )


@site.route('/about', methods=['GET', 'POST'])
@login_required
def about():
    return render_template(
        'site/about.html'
        )


@site.route('/contact', methods=['GET', 'POST'])
@login_required
def contact():
    return render_template(
        'site/contact.html'
        )


@site.route('/categories/<category>', methods=['GET', 'POST'])
@login_required
def category(category):

    c = CategoryWithPlants()
    info = c.get(category)

    #print(info)

    return render_template(
        'site/categories.html', 
        name=category,
        info=info
        )
