



(function() {

    $('.plant-img img').each(function() {
        enhanced = $(this).attr('data_photo_360');
        $(this).attr('src', enhanced) 
    });

    $('.list-toggle').on('click', function() {
        var target = $('#' + $(this).attr('data-toggles'));

        $('.list-row-list').addClass('inactive');
        $(target).removeClass('inactive');

        $('.list-toggle').each(function() {
          $(this).parent().addClass('inactive');
          $(this).removeClass('active');
        });

        $(this).addClass('active');
        $(this).parent().removeClass('inactive');
    });

}());


(function() {

    $('img.enhance').each(function() {
        enhanced = $(this).attr('data_photo_360');
        if (typeof(enhanced) != 'undefined') {
            $(this).attr('src', enhanced);
        }
    });
    
}());
