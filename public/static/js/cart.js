
//class Cart {

    //constructor() {

        //let self = this;

        //this.cart_data = {
            //items: []
        //};

        //new Vue({
            //el: '#cart',
            //data: self.cart_data,
            //delimiters: ['[[', ']]'],
            //computed: {
                //totalCartItems: () => {
                    //return this.cart_data.items.reduce((total, i) => total + i.qty, 0)
                //},
                //totalCartValue: () => {
                    //return this.cart_data.items.reduce((total, i) => total + i.price, 0)
                //},
            //},
            //methods: {
                //checkout: (e) => {
                    //console.log(self.cart_data.items[0].qty);
                //}
            //}
        //})
    //}

    //addItem(i) {
        //this.cart_data.items.push(i);
        //this.cart_data.count_available -= i.qty;
    //};

    //checkOut() {
    //};
//}

//const CART = new Cart();

new Vue({
    el: '#top-search',
    delimiters: ['[[', ']]'],
    data: {
        show_search: false,
        show_result: false,
        keyword: '',
        results: [],
        //result_pos: CART.search_result_pos 
    },
    methods: {
        toggleSearch: function() {
            if (this.show_search || this.show_result) {
                this.show_search = false;
                this.show_result = false;
                this.keyword = '';
            }
            else {
                this.show_search = true;
                this.show_result = true;
            }
        },
        autoShowResults: function(event) {
            if (this.keyword.length > 3) {
                this.showResults();
            }
            else {
                this.show_result = false
            }
        },
        showResults: function() {

            $.get('/api/site_search/' + this.keyword, (rslt) => {
                this.results = rslt;
                this.show_result = true
            });

        },
    },
    computed: {
        resultPosition: function() {
            var width = $('#top-search-input').width();
            return {
                width: width + 50 + 'px'
            }
        }
    }

});


(function() {

    $('.cloaked').css('visibility', 'visible');


}());


