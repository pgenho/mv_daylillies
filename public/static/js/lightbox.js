;(function() {

    var lbx = $('#photo-lightbox');
    
    $('.lightbox').on('click', function() {

        console.log($(this).attr('data_photo_720'));

        $(lbx)
            .find('img')
            .remove();

        var close=$('<div class="button">x</div>')

        var img = $('<img></img>');
        $(img).attr('src', $(this).attr('data_photo_720'));

        $(lbx)
            .append(close, img)
            .css('display', 'flex')

        $(close).on('click', function() {
            $(lbx)
              .empty()
              .hide()
        });

        $(img).on('click', function() {
            $(lbx)
              .empty()
              .hide()
        });

    });

}())
